import loadCourses from "./courses/loadCourses.js"
import addModalToHtml from "./v1.5/modal/addModalToHTML.js";
import loadTable from "./v1.5/table/loadTable.js";
import onBtnCreateCourse from "./v1.5/modal/create/createEvent.js";
import { readURL } from "./constants.js";
import { onBtnUpdateCourse, onIconEditClick } from "./v1.5/modal/update/event.js";
import { onBtnDeleteCourse, onIconDeleteClick } from "./v1.5/modal/delete/event.js";
/*** REGION 2 */
const start = () => {
    addModalToHtml()
    loadCourses()
    loadTable()

    // THÊM MỚI SẢN PHẨM
    //gán sự kiện ấn nút thêm khóa học
    $('#create-courses').on('click', () => {
        console.log('test');
        $('#create-course-modal').modal('show')
    })
    //gán sự kiện ấn nút thêm mới sản phẩm
    $('#btn-create-course').on('click', function () {
        onBtnCreateCourse()
    })
    //xử lý sự kiện khi chọn ảnh trên modal thêm sản phẩm
    $("#img-new-course").change(function () {
        readURL(this, $('#img-preview-new-course'));
    });
    $('#img-new-teacher').change(function () {
        readURL(this, $('#img-preview-new-teacher'));
    });

    // XEM CHI TIẾT & CẬP NHẬT SẢN PHẨM
    //gán sự kiện ấn nút cập nhật khóa học
    $('#tbody-courses').on('click', '.edit-course', function () {
        console.log('test');
        $('#update-course-modal').modal('show')
        onIconEditClick(this)
    })
    //gán sự kiện ấn nút cập nhật sản phẩm
    $('#btn-update-course').on('click', function () {
        onBtnUpdateCourse()
    })
    //xử lý sự kiện khi chọn ảnh trên modal cập nhật
    $("#inp-img-course").change(function () {
        readURL(this, $('#img-preview-course'));
    });
    $('#inp-img-teacher').change(function () {
        readURL(this, $('#img-preview-teacher'));
    });

    // XÓA SẢN PHẨM
    //gán sự kiện ấn nút xóa khóa học
    $('#tbody-courses').on('click', '.delete-course', function () {
        console.log('test');
        $('#delete-course-modal').modal('show')
        onIconDeleteClick(this)
    })
    $('#btn-delete-course').on('click', () => {
        onBtnDeleteCourse()
    })
}
start()