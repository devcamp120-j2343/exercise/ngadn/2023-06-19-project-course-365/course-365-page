/*** REGION 4 */
const courseCard = (paramCourse) => {
    return (`
        <li class="col-md-3 pl-0 py-2">
            <div class="card" style="height:100%;">
                <img src="${paramCourse.coverImage}" class="card-img-top">
                <div class="card-body pb-0">
                    <h5 class="card-title">${paramCourse.courseName}</h5>
                    <div class="d-flex">
                        <span>
                            <i class="far fa-clock"></i>
                            ${paramCourse.duration}
                        </span>
                        <span class="mx-3">${paramCourse.level}</span>
                    </div>
                    <div class="d-flex pt-3 pb-4">
                        <span>$${paramCourse.discountPrice}</span>
                        <span class="ml-2 text-secondary" style="text-decoration: line-through;">$${paramCourse.price}</span>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item d-flex align-items-center justify-content-between"
                        style="background: rgba(0, 0, 0, 0.03);">
                        <div class="d-flex align-items-center">
                            <img src="${paramCourse.teacherPhoto}" style="border-radius:50%; width:20%;height:20%">
                            <span class="mx-2">${paramCourse.teacherName}</span>
                        </div>
                        <i class="far fa-bookmark"></i>
                    </li>
                </ul>
            </div>
        </li>
    `)
}
export default courseCard