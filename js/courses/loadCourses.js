import courseCard from "./formCourses.js";
import { gCourses } from "../v1.5/callAPI.js";


/*** REGION 4 */
// Hàm load danh sách các khóa học theo tiêu đề
const loadCourses = () => {
    loadRecommendCourses()
    loadPopularCourses()
    loadTrendingCourses()
}
// Hàm load khóa học recommend
const loadRecommendCourses = () => {
    var vRecommendCourses = gCourses.splice(0, 4)
    mapCourses($('#recommend-cards'), vRecommendCourses)
}
// Hàm load khóa học popular
const loadPopularCourses = () => {
    var vPopularCourses = gCourses.filter(course => course.isPopular == true)
    mapCourses($('#popular-cards'), vPopularCourses)
}
// Hàm load khóa học trending
const loadTrendingCourses = () => {
    var vTrendingCourses = gCourses.filter(course => course.isTrending == true)
    mapCourses($('#trending-cards'), vTrendingCourses)
}


// Hàm map qua danh sách các khóa học và render ra giao diện
const mapCourses = (paramUl, paramCourses) => {
    paramCourses.map(course => paramUl.append(courseCard(course)))
}


export default loadCourses