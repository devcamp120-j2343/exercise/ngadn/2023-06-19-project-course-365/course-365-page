/*** REGION 1 */


export const gURL = 'https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/courses/'

var gSTT = 0
export const gTable = $('#table-courses').DataTable({
    columns: [
        { data: 'id' },
        { data: 'courseCode' },
        { data: 'teacherName' },
        { data: 'level' },
        { data: 'duration' },
        { data: 'price' },
        { data: 'discountPrice' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0,
            render: () => {
                gSTT++
                return gSTT
            }
        },
        {
            targets: 4,
            className: 'text-center'
        },
        {
            targets: 5,
            className: 'text-center'
        },
        {
            targets: 6,
            className: 'text-center'
        },
        {
            targets: 7,
            className: 'text-center',
            defaultContent: `
            <i class="fa-solid fa-pen-to-square edit-course"  data-toggle="tooltip" data-placement="bottom" title="Xem chi tiết" style="color: #4f9037; cursor:pointer;"></i>
            <i class="fa-solid fa-trash-can delete-course"  data-toggle="tooltip" data-placement="bottom" title="Xóa khóa học" style="color: #981017; cursor:pointer;"></i>
            `
        }
    ]
})

//hàm xử lý khi chọn ảnh
export function readURL(paramInput, paramImg) {
    if (paramInput.files && paramInput.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            paramImg.css('background-image', 'url(' + e.target.result + ')');
            paramImg.css('height', 290);
            paramImg.hide();
            paramImg.fadeIn(650);
        }
        reader.readAsDataURL(paramInput.files[0]);
    }
}

export const getDetailFromIcon = (paramIcon) => {
    var vRow = $(paramIcon).parents('tr')
    var vDetail = gTable.row(vRow).data()
    return vDetail
}