import { gURL } from "../../../constants.js";

const onBtnCreateCourse = () => {
    //B1: thu thập dữ liệu
    var vCouse = {}
    vCouse = getCourseInp()
    //B2: validate dữ liệu
    if (validateCourse(vCouse)) {
        if (validateCourseImg(vCouse)) {
            callAPIToCreateCourse(vCouse)
            console.log(vCouse);
        }
    }
}

const getCourseInp = () => {
    return (
        {
            courseCode: $('#inp-new-course-code').val(),
            courseName: $('#inp-new-course-name').val(),
            teacherName: $('#inp-new-teacher-name').val(),
            level: $('#select-new-level').val(),
            duration: $('#inp-new-duration').val(),
            price: $('#inp-new-price').val(),
            discountPrice: $('#inp-new-discount-price').val(),
            coverImage: $('#img-new-course').val(),
            teacherPhoto: $('#img-new-teacher').val()
        }
    )
}

export const validateCourse = (paramCourse) => {
    if (paramCourse.courseCode.length < 10) {
        alert('Hãy nhập mã khóa học chứa ít nhất 10 ký tự')
        return false
    }
    if (paramCourse.courseName.length < 20) {
        alert('Hãy nhập mô tả khóa học chứa ít nhất 20 ký tự')
        return false
    }
    if (paramCourse.teacherName == '') {
        alert('Hãy nhập tên giáo viên phụ trách khóa học')
        return false
    }
    if (paramCourse.level == '0') {
        alert('Hãy chọn cấp độ của khóa')
        return false
    }
    if (paramCourse.duration == '') {
        alert('Hãy chọn thời lượng của khóa')
        return false
    }
    if (paramCourse.price <= 0) {
        alert('Giá gốc khóa học phải là số nguyên lớn hơn 0')
        return false
    }
    if (paramCourse.discountPrice <= 0 || paramCourse.discountPrice > paramCourse.price) {
        alert('Giá giảm của khóa học phải thấp hơn hoặc bằng giá gốc')
        return false
    }
    return true
}

const validateCourseImg = (paramCourse) => {
    if (paramCourse.coverImage == '') {
        alert('Hãy chọn hình ảnh đại diện của khóa học')
        return false
    }
    if (paramCourse.teacherPhoto == '') {
        alert('Hãy chọn hình đại diện của giáo viên')
        return false
    }
    return true
}

const callAPIToCreateCourse = (paramCourse) => {
    $.ajax({
        url: gURL,
        type: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(paramCourse),
        success: res => handleCreateCourse(res),
        error: err => console.log(err)
    })
}

const handleCreateCourse = (paramRes) => {
    alert('Đã tạo mới thành công khóa học' + ' ' + paramRes.courseCode)
    location.reload()
}

export default onBtnCreateCourse