import { gURL, getDetailFromIcon } from "../../../constants.js";
import { validateCourse } from "../create/createEvent.js";

var gId;

const onIconEditClick = (paramIcon) => {
    var vDetail = getDetailFromIcon(paramIcon)
    gId = vDetail.id
    $('#inp-course-code').val(vDetail.courseCode)
    $('#inp-course-name').val(vDetail.courseName)
    $('#inp-teacher-name').val(vDetail.teacherName)
    $('#select-level').val(vDetail.level)
    $('#inp-duration').val(vDetail.duration)
    $('#inp-price').val(parseInt(vDetail.price))
    $('#inp-discount-price').val(parseInt(vDetail.discountPrice))
    $('#img-preview-course').attr('src', vDetail.coverImage)
    $('#img-preview-teacher').attr('src', vDetail.teacherPhoto)
}

const onBtnUpdateCourse = () => {
    var vCourse = {}
    vCourse = getCourse()
    if (validateCourse(vCourse)) {
        callAPIToUpdateCourse(vCourse, gId)
    }
}

const getCourse = () => {
    return (
        {
            courseCode: $('#inp-course-code').val(),
            courseName: $('#inp-course-name').val(),
            teacherName: $('#inp-teacher-name').val(),
            level: $('#select-level').val(),
            duration: $('#inp-duration').val(),
            price: $('#inp-price').val(),
            discountPrice: $('#inp-discount-price').val(),
            coverImage: $('#img-preview-course').attr('src'),
            teacherPhoto: $('#img-preview-teacher').attr('src')
        }
    )
}

const callAPIToUpdateCourse = (paramCourse, paramId) => {
    $.ajax({
        url: gURL + paramId,
        type: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(paramCourse),
        success: res => handleUpdate(res),
        error: err => console.log(err)
    })
}

const handleUpdate = (paramRes) => {
    alert('Đã cập nhật thành công khóa học' + ' ' + paramRes.courseCode)
    location.reload()
}

export { onIconEditClick, onBtnUpdateCourse }