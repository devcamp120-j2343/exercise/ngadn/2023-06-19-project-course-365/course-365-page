const updateModal = () => {
    return (`
    <div id="update-course-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Chi tiết khóa học</h5>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body d-flex align-items-center">
                    <form class="col-md-7">
                        <div class="row my-3">
                            <label class="col-md-5">Mã khóa học</label>
                            <input type="text" id="inp-course-code" placeholder="Mã khóa học..."
                                class="col-md-7 form-control">
                        </div>
                        <div class="row my-3">
                            <label class="col-md-5">Mô tả</label>
                            <input type="text" id="inp-course-name" placeholder="Mô tả..."
                                class="col-md-7 form-control">
                        </div>
                        <div class="row my-3">
                            <label class="col-md-5">Giáo viên</label>
                            <input type="text" id="inp-teacher-name" placeholder="Tên giáo viên..."
                                class="col-md-7 form-control">
                        </div>
                        <div class="row my-3">
                            <label class="col-md-5">Cấp độ</label>
                            <select id="select-level" class="col-md-7 form-control">
                                <option value="0" selected>Chọn cấp độ</option>
                                <option value="Beginner">Beginner</option>
                                <option value="Intermediate">Intermediate</option>
                                <option value="Advanced">Advanced</option>
                                
                            </select>
                        </div>
                        <div class="row my-3">
                            <label class="col-md-5">Thời lượng</label>
                            <input type="text" id="inp-duration" placeholder="Thời lượng..."
                                class="col-md-7 form-control">
                        </div>
                        <div class="row my-3">
                            <label class="col-md-5">Giá gốc</label>
                            <input type="number" id="inp-price" placeholder="Giá gốc..."
                                class="col-md-7 form-control">
                        </div>
                        <div class="row my-3">
                            <label class="col-md-5">Giá giảm</label>
                            <input type="number" id="inp-discount-price" placeholder="Giá giảm..."
                                class="col-md-7 form-control">
                        </div>
                    </form>
                    <div class="col-md-5">
                        <div class="form-group" style="width:100%; height:50%">
                            <div style="width:100%; height:20%">
                                <label>Đổi ảnh đại diện khóa học</label>
                                <input  type="file" id="inp-img-course">
                            </div>
                            <img id="img-preview-course" style="width:100%; height:80%" alt="...a">
                        </div>
                        <div class="form-group" style="width:100%; height:50%">
                            <div style="width:100%; height:20%">
                                <label>Đổi ảnh đại diện giáo viên</label>
                                <input  type="file" id="inp-img-teacher">
                            </div>
                            <img id="img-preview-teacher" style="width:100%; height:80%" alt="...b">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="btn-update-course">Cập nhật</button>
                    <button class="btn btn-secondary" id="btn-update-cancel" data-dismiss="modal">Hủy bỏ</button>
                </div>
            </div>
        </div>
    </div>
    `)
}


export default updateModal