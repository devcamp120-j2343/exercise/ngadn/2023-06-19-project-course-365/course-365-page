import { gURL, getDetailFromIcon } from "../../../constants.js"

var gId, gCourseCode
const onIconDeleteClick = (paramIcon) => {
    //thu thập thông tin cần xóa
    var vDetail = getDetailFromIcon(paramIcon)
    gId = vDetail.id
    gCourseCode = vDetail.courseCode
}
const onBtnDeleteCourse = () => {
    $.ajax({
        url: gURL + gId,
        type: 'DELETE',
        success: handleDelete,
        error: err => console.log(err)
    })
}
const handleDelete = () => {
    alert('Đã xóa thành công khóa học ' + gCourseCode)
    location.reload()
    $('#delete-course-modal').modal('hide')
}
export { onIconDeleteClick, onBtnDeleteCourse }