const deleteModal=()=>{
    return (`
    <div class="modal fade" tabindex="-1" id="delete-course-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="h5-modal-title">Xác nhận xóa khóa học</h5>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label>Bạn có chắc chắn muốn xóa khóa học này không?</label>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" id="btn-delete-course">Confirm</button>
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
`)
}
export default deleteModal