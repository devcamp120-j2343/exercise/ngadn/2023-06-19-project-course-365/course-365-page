import createModal from "./create/createModal.js";
import deleteModal from "./delete/modal.js";
import updateModal from "./update/modal.js";
const addModalToHtml = () => {
    $('#create-modal').append(createModal())
    $('#update-modal').append(updateModal())
    $('#delete-modal').append(deleteModal())
}
export default addModalToHtml