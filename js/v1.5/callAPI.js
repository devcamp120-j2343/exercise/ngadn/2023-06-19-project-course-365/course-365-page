import { gURL } from "../constants.js"

const callAPIToLoadCourses = () => {
    var vCourses = []
    $.ajax({
        url: gURL,
        type: 'GET',
        async: false,
        dataType: 'json',
        success: res => vCourses = res,
        error: err => console.log(err)
    })
    return vCourses
}
const gCourses = callAPIToLoadCourses()

export { gCourses }