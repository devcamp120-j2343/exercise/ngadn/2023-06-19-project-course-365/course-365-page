import { gTable } from "../../constants.js"
import { gCourses } from "../callAPI.js"

const loadTable = () => {
    gTable.clear()
    gTable.rows.add(gCourses)
    gTable.draw()
}
export default loadTable